import * as nodemailer from 'nodemailer';

export interface IMailConfig {
    from: string;
    transport: {
        auth?: {
            pass: string;
            user: string;
        };
        host: string;
        port: number;
        secure: boolean;
    };
}
export interface IMailOptions {
    from?: string;
    sender?: string;
    to?: string | string[];
    cc?: string | string[];
    bcc?: string | string[];
    replyTo?: string;
    inReplyTo?: string;
    references?: string | string[];
    subject?: string;
    text?: string;
    html?: string;
    watchHtml?: string;
    icalEvent?: string;
    messageId?: string;
    date?: Date | string;
    encoding?: string;
    raw?: string;
    disableUrlAccess?: boolean;
    disableFileAccess?: boolean;
}
export class Mail {
    protected from: string;
    protected transport: {
        auth?: {
            pass: string;
            user: string;
        },
        host: string;
        port: number;
        secure: boolean;
    };
    
    constructor(config: IMailConfig) {
        this.from = config.from;
        this.transport = config.transport;
    }
    
    /**
     * Sends an email message
     */
    public sendMail(options: IMailOptions): Promise<nodemailer.SentMessageInfo> {
        return new Promise((resolve, reject) => {
            if (!options.from) {
                options.from = this.from;
            }
            if (!options.to) {
                return reject({
                    message: 'Missing recipient for email',
                });
            }
            
            // create reusable transporter object using the default SMTP transport
            const transporter = nodemailer.createTransport(this.transport);
            
            // send mail with defined transport object
            return transporter.sendMail(options).then((info) => {
                return resolve(Object.assign({}, info, {
                    url: nodemailer.getTestMessageUrl(info),
                }));
            }).catch(reject);
        });
    }
}

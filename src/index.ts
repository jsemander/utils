import { Request } from 'express';
import { Model } from 'mongoose';
import { IPagination } from './Types';

export * from './Config';
export * from './Cron';
export * from './Job';
export * from './JWT';
export * from './Log';
export * from './Mail';
export * from './Resolver';
export * from './Types';

/**
 * Calculates the skip amount
 */
export const calculateSkip = (page: number, limit: number): number => (page - 1) * limit;

/**
 * Converts boolean string values to boolean values
 */
export const booleanSanitizer = (value: string, options?: { req: Request; location: string; path: string }) => {
    if (value === '1' || value === 'true') {
        return true;
    } else if (value === '0' || value === 'false') {
        return false;
    } else {
        return value;
    }
};

/**
 * Converts number string values to number values
 */
export const numberSanitizer = (value: string, options?: { req: Request; location: string; path: string }) => {
    if (!isNaN(Number(value))) {
        return Number(value);
    }
    return value;
};

/**
 * Validates that the value supplied is a date
 */
export const dateValidator = (value: string, options?: { req: Request; location: string; path: string }) => {
    return /^[\d]{4}\-[\d]{2}\-[\d]{2}T[\d]{2}:[\d]{2}:[\d]{2}(|\.[\d]{3})(-[\d]{2}:[\d]{2}|Z)$/.test(`${value}`);
};

/**
 * Validates that the value supplied is a number
 */
export const numberValidator = (value: string, options?: { req: Request; location: string; path: string }) => {
    return /^[-\d\.]+$/.test(`${value}`) || !isNaN(Number(value));
};

/**
 * Paginates the records
 */
export const paginateRecords = (
    model: Model<any>,
    params: any,
    sort: any,
    lookups: any[],
    unwinds: any[],
    page: number,
    limit: number,
): Promise<IPagination<any>> => {
    const skip = calculateSkip(page, limit);
    return model.countDocuments(params).then((total: number) => {
        const aggreagation = model.aggregate().match(model.where(params).cast(model));
        lookups.forEach((lookup) => aggreagation.lookup(lookup));
        unwinds.forEach((unwind) => aggreagation.unwind(unwind));
        aggreagation.sort(sort).skip(skip);
        if (limit > 0) {
            aggreagation.limit(limit);
        }
        return aggreagation.then((docs: any[]) => {
            return {
                data: docs,
                meta: {
                    data: {},
                    pagination: {
                        count: docs.length,
                        total,
                    },
                },
            };
        });
    });
};

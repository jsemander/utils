import * as parser from 'cron-parser';
import { head, isEmpty } from 'lodash';
import * as moment from 'moment-timezone';
import { Document, Schema, Types } from 'mongoose';
import * as os from 'os';

import { paginateRecords } from '.';
import { Resolver } from './Resolver';
import { IPagination, IPaginationOptions } from './Types';

export interface IJob {
    _id: Types.ObjectId;
    data: any;
    enabled: boolean;
    lastFinishedAt: Date;
    lastModifiedBy: string;
    lastRunAt: Date;
    name: string;
    nextRunAt: Date;
    repeatInterval: string;
    repeatTimezone: string;
    task: {
        model: string;
        action: string;
    };
}
export interface IJobModel extends Document {
    data: any;
    enabled: boolean;
    lastFinishedAt: Date;
    lastModifiedBy: string;
    lastRunAt: Date;
    name: string;
    nextRunAt: Date;
    repeatInterval: string;
    repeatTimezone: string;
    task: {
        model: string;
        action: string;
    };
}
export interface IJobCreateParams {
    data?: any;
    name?: string;
    nextRunAt?: string;
    repeatInterval?: string;
    repeatTimezone?: string;
    task: {
        model: string;
        action: string;
    };
}
export interface IJobListParams {
    enabled?: boolean;
}
export interface IJobRemoveParams {
    _id?: string;
    nextRunAt?: {
        $eq?: Types.ObjectId | null;
        $ne?: Types.ObjectId | null;
    };
}
export interface IJobUpdateParams {
    data?: any;
    name?: string;
    nextRunAt?: string;
    repeatInterval?: string;
    repeatTimezone?: string;
    task?: {
        model: string;
        action: string;
    };
}
export const JobSchema = new Schema({
    data: {
        default: {},
        type: Object,
    },
    enabled: {
        default: true,
        type: Boolean,
    },
    lastFinishedAt: {
        default: null,
        type: Date,
    },
    lastModifiedBy: {
        default: null,
        type: String,
    },
    lastRunAt: {
        default: null,
        type: Date,
    },
    name: {
        type: String,
    },
    nextRunAt: {
        default: null,
        type: Date,
    },
    repeatInterval: {
        default: null,
        type: String,
    },
    repeatTimezone: {
        default: 'America/New_York',
        type: String,
    },
    task: {
        action: {
            type: String,
        },
        model: {
            type: String,
        },
    },
}, {
    timestamps: true,
    versionKey: false,
});
export class Job<Models extends { job: any }> extends Resolver<Models> {
    /**
     * Create job
     */
    public create(data: IJobCreateParams): Promise<IJobModel> {
        const params: any = {
            repeatInterval: data.repeatInterval,
            task: data.task,
        };
        if (!isEmpty(data.data)) {
            params.data = data.data;
        }
        return this.models.job.findOne(params).then((result: IJobModel | null) => {
            if (result) {
                return Promise.reject<any>({
                    message: 'A job already exists by the specified configuration',
                    statusCode: 409,
                });
            }
            const job = new this.models.job(data);
            return job.save();
        });
    }
    
    /**
     * Disables a job
     */
    public disable(id: string): Promise<IJobModel> {
        return this.models.job.findById(id).then((job: IJobModel) => {
            if (!job) {
                return Promise.reject<IJobModel>({
                    message: 'No job found',
                    statusCode: 404,
                });
            }
            return job.set({ enabled: false }).save();
        });
    }
    
    /**
     * Mark job as finished
     */
    public finish(id: string): Promise<IJobModel> {
        return this.models.job.findById(id).then((job: IJobModel) => {
            if (!job) {
                return Promise.reject<IJobModel>({
                    message: 'No job found',
                    statusCode: 404,
                });
            }
            if (job.nextRunAt) {
                return job.set({ lastFinishedAt: moment().toISOString() }).save();
            } else {
                return this.remove({ _id: id });
            }
        });
    }
    
    /**
     * Get job by id
     */
    public get(id: string): Promise<IJobModel | null> {
        return this.models.job.findById(id);
    }
    
    /**
     * Get next job to run
     */
    public getNext(): Promise<IJobModel | null> {
        return this.models.job.find({
            enabled: true,
            nextRunAt: {
                $lte: moment().toISOString(),
            },
        }).sort({
            nextRunAt: 1,
        }).then((jobs: IJobModel[]) => {
            return head(jobs);
        });
    }
    
    /**
     * Retrieves a list of jobs by params
     */
    public list(params: IJobListParams, options: IPaginationOptions): Promise<IPagination<IJobModel>> {
        return paginateRecords(this.models.job, params, options.sort, [], [], options.page, options.limit);
    }
    
    /**
     * Delete job by params
     */
    public remove(params: IJobRemoveParams): Promise<boolean> {
        return this.models.job.deleteMany(params).then(() => true);
    }
    
    /**
     * Mark job as running
     */
    public running(id: string): Promise<IJobModel> {
        return this.models.job.findById(id).then((job: IJobModel) => {
            if (!job) {
                return Promise.reject<IJobModel>({
                    message: 'No job found',
                    statusCode: 404,
                });
            }
            if (job.repeatInterval) {
                const options: { tz?: string; } = {};
                if (job.repeatTimezone) {
                    options.tz = job.repeatTimezone;
                }
                const interval = parser.parseExpression(job.repeatInterval, options);
                return job.set({
                    lastModifiedBy: `${os.hostname()} - ${process.pid}`,
                    lastRunAt: moment().toISOString(),
                    nextRunAt: interval.next().toISOString(),
                }).save();
            } else {
                return job.set({
                    lastModifiedBy: `${os.hostname()} - ${process.pid}`,
                    lastRunAt: moment().toISOString(),
                    nextRunAt: null,
                }).save();
            }
        });
    }
    
    /**
     * Update job by id
     */
    public update(id: string, data: IJobUpdateParams): Promise<IJobModel> {
        return this.models.job.findById(id).then((record: IJobModel | null) => {
            // Check if the record was found
            if (!record) {
                return Promise.reject<any>({
                    message: 'No job found',
                    statusCode: 404,
                });
            }
            return this.models.job.findOne({
                _id: { $ne: id },
                repeatInterval: data.repeatInterval,
                task: data.task,
            }).then((result: IJobModel | null) => {
                if (result) {
                    return Promise.reject<any>({
                        message: 'A job already exists by the specified configuration',
                        statusCode: 409,
                    });
                }
                
                // Set the job
                record.set(data);
                
                // Return the job after saving
                return record.save().then(() => this.get(id));
            });
        });
    }
}

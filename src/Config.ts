import { Document, Schema } from 'mongoose';

import { Resolver } from './Resolver';

export interface IConfigModel extends Document {
    name: string;
    value: any;
}
export const ConfigSchema = new Schema({
    name: {
        type: String,
    },
    value: {
        type: Schema.Types.Mixed,
    },
}, {
    timestamps: true,
    versionKey: false,
});
export class Config<Models extends { config: any }> extends Resolver<Models> {
    /**
     * Get config by name
     */
    public getByName(name: string): Promise<IConfigModel> {
        return this.models.config.findOne({ name }).then((config: IConfigModel) => {
            if (!config) {
                return Promise.reject<IConfigModel>({
                    message: 'No config found',
                    statusCode: 404,
                });
            }
            return config;
        });
    }
    
    /**
     * Retrieves a list of configs by params
     */
    public list(params: any): Promise<IConfigModel[]> {
        return this.models.config.find(params);
    }
}

import { Document, Schema, Types } from 'mongoose';

import { paginateRecords } from '.';
import { Resolver } from './Resolver';
import { IDateRange, IPagination, IPaginationOptions } from './Types';

export interface ILog {
    _id: Types.ObjectId;
    action: string;
    data: any;
    entity: any;
    entity_name: string;
    error: any;
    level: string;
    type: string;
    updates: ILogUpdate[];
    user: any;
}
export interface ILogModel extends Document {
    [key: string]: any;
    action: string;
    data: any;
    entity?: any;
    entity_name: string;
    error: any;
    level: string;
    type: string;
    updates: ILogUpdate[];
    user?: any;
}
export interface ILogCreateParams {
    action: string;
    data?: any;
    entity?: any;
    entity_name: string;
    error?: any;
    level?: string;
    type: string;
    updates: ILogUpdate[];
    user?: any;
}
export interface ILogUpdate {
    field_name: string;
    new_value: any;
    old_value: any;
}
export interface ILogListParams {
    action?: string;
    createdAt: IDateRange;
    entity_name?: string | { [key: string]: any; };
    level?: string;
    type?: string;
    user?: {
        $eq?: string;
        $in?: string[];
    };
}
export const LogUpdateSchema = new Schema({
    field_name: {
        type: String,
    },
    new_value: {
        type: Schema.Types.Mixed,
    },
    old_value: {
        type: Schema.Types.Mixed,
    },
}, {
    _id: false,
    versionKey: false,
});
export const LogSchema = new Schema({
    action: {
        required: true,
        type: String,
    },
    data: {
        default: {},
        required: true,
        type: Schema.Types.Mixed,
    },
    entity: {
        default: {},
        type: Schema.Types.Mixed,
    },
    entity_name: {
        required: true,
        type: String,
    },
    error: {
        default: {},
        type: Schema.Types.Mixed,
    },
    level: {
        default: 'data',
        required: true,
        type: String,
    },
    type: {
        default: 'info',
        required: true,
        type: String,
    },
    updates: [LogUpdateSchema],
    user: {
        default: null,
        ref: 'User',
        type: Schema.Types.ObjectId,
    },
}, {
    timestamps: true,
    versionKey: false,
});
export class Log<Models extends { log: any }> extends Resolver<Models> {
    /**
     * Create log
     */
    public create(data: ILogCreateParams): Promise<ILogModel> {
        const log = new this.models.log(data);
        switch (data.entity_name) {
            case 'api':
            case 'cron':
            case 'email':
            case 'job':
            case 'system':
                log.level = 'system';
                break;
            default:
                log.level = 'data';
        }
        return log.save().then((record: ILogModel) => this.get(record._id));
    }
    
    /**
     * Get log by id
     */
    public get(id: string): Promise<ILogModel> {
        return this.models.log.findById(id).then((log: ILogModel) => {
            if (!log) {
                return Promise.reject<ILogModel>({
                    message: 'No log found',
                    statusCode: 404,
                });
            }
            return log;
        });
    }
    
    /**
     * Retrieves a list of logs by params
     */
    public list(params: ILogListParams, options: IPaginationOptions): Promise<IPagination<ILogModel>> {
        return paginateRecords(this.models.log, params, options.sort, [{
            as: 'user',
            foreignField: '_id',
            from: 'users',
            localField: 'user',
        }], [{
            path: '$user',
            preserveNullAndEmptyArrays: true,
        }], options.page, options.limit);
    }
}

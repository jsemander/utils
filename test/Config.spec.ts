import { assert, expect } from 'chai';
import 'mocha';
import { MongoMemoryServer } from 'mongodb-memory-server';
import * as mongoose from 'mongoose';

import { Config, ConfigSchema, IConfigModel } from '../src/Config';

let config: Config<{
    config: mongoose.Model<IConfigModel>;
}>;
let configModel: mongoose.Model<IConfigModel>;
const mongod = new MongoMemoryServer({
    autoStart: false,
});

describe('Config', () => {
    before(() => {
        return mongod.start().then(() => {
            return mongod.getConnectionString().then((uri) => {
                return new Promise((resolve, reject) => {
                    mongoose.connect(uri, {
                        useNewUrlParser: true,
                    }, (err) => {
                        if (err) {
                            return reject(err);
                        }
                        configModel = mongoose.model('Config', ConfigSchema);
                        const record = new configModel({
                            name: 'Name',
                            value: '@ghw/utils Test',
                        });
                        record.save();
                        config = new Config({
                            config: configModel,
                        });
                        return resolve();
                    });
                });
            });
        });
    });
    after(() => mongoose.disconnect().then(() => mongod.stop()));
    afterEach(() => configModel.deleteMany({}));
    describe('#getByName()', () => {
        it('should retrieve a list of configs successfully', () => {
            return config.getByName('Name').then((record) => {
                expect(record).to.have.property('name', 'Name');
                expect(record).to.have.property('value', '@ghw/utils Test');
            });
        });
        it('should not retrieve a list of configs successfully', () => {
            return config.getByName('Fake').then(() => {
                assert.fail('Config should not have been found');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No config found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
    });
    describe('#list()', () => {
        it('should retrieve a list of configs successfully', () => {
            return config.list({});
        });
    });
});

import { expect } from 'chai';
import 'mocha';

import { booleanSanitizer, calculateSkip, dateValidator, numberSanitizer, numberValidator } from '../src';

describe('#booleanSanitizer', () => {
    it('should not sanitize the string to boolean', () => {
        expect(booleanSanitizer('a')).to.be.equal('a');
    });
    it('should sanitize the string to boolean: true', () => {
        expect(booleanSanitizer('true')).to.be.equal(true);
    });
    it('should sanitize the string to boolean: false', () => {
        expect(booleanSanitizer('false')).to.be.equal(false);
    });
    it('should sanitize the string to boolean: 1', () => {
        expect(booleanSanitizer('1')).to.be.equal(true);
    });
    it('should sanitize the string to boolean: 0', () => {
        expect(booleanSanitizer('0')).to.be.equal(false);
    });
});
describe('#dateValidator', () => {
    it('should not validate the string as a number', () => {
        expect(dateValidator('2019-03-04 00:00:00')).to.be.equal(false);
    });
    it('should validate the string as a number', () => {
        expect(dateValidator('2019-03-04T00:00:00Z')).to.be.equal(true);
    });
    it('should validate the string as a number', () => {
        expect(dateValidator('2019-03-04T00:00:00-04:00')).to.be.equal(true);
    });
    it('should validate the string as a number', () => {
        expect(dateValidator('2019-03-04T00:00:00.000-04:00')).to.be.equal(true);
    });
});
describe('#numberSanitizer', () => {
    it('should not sanitize the string to number', () => {
        expect(numberSanitizer('a')).to.be.equal('a');
    });
    it('should sanitize the string to number', () => {
        expect(numberSanitizer('1')).to.be.equal(1);
    });
});
describe('#numberValidator', () => {
    it('should not validate the string as a number', () => {
        expect(numberValidator('a')).to.be.equal(false);
    });
    it('should validate the string as a number', () => {
        expect(numberValidator('1')).to.be.equal(true);
    });
});
describe('#calculateSkip', () => {
    it('should calculate the skip', () => {
        const limit = 10;
        const page = 2;
        const skip = 10;
        expect(calculateSkip(page, limit)).to.be.equal(skip);
    });
});

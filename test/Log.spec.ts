import { assert, expect } from 'chai';
import 'mocha';
import * as moment from 'moment-timezone';
import { MongoMemoryServer } from 'mongodb-memory-server';
import * as mongoose from 'mongoose';

import { ILogModel, Log, LogSchema } from '../src/Log';

let log: Log<{
    log: mongoose.Model<ILogModel>;
}>;
let logModel: mongoose.Model<ILogModel>;
const mongod = new MongoMemoryServer({
    autoStart: false,
});

describe('Log', () => {
    before(() => {
        return mongod.start().then(() => {
            return mongod.getConnectionString().then((uri) => {
                return new Promise((resolve, reject) => {
                    mongoose.connect(uri, {
                        useNewUrlParser: true,
                    }, (err) => {
                        if (err) {
                            return reject(err);
                        }
                        logModel = mongoose.model('Log', LogSchema);
                        log = new Log({
                            log: logModel,
                        });
                        return resolve();
                    });
                });
            });
        });
    });
    after(() => mongoose.disconnect().then(() => mongod.stop()));
    afterEach(() => logModel.deleteMany({}));
    describe('#get()', () => {
        it('should find a record', () => {
            return log.create({
                action: 'shutdown',
                data: {},
                entity: null,
                entity_name: 'users',
                error: {},
                type: 'error',
                updates: [],
            }).then((l) => {
                log.get(l._id).then((l2: ILogModel) => {
                    expect(l2).to.have.property('entity_name', 'users');
                    expect(l2).to.have.property('level', 'data');
                });
            });
        });
        it('should not find any record', () => {
            return log.get('5a3c861b6d4ef006979bf674').then(() => {
                assert.fail('Log should not have been found');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No log found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
    });
    describe('#list()', () => {
        it('should find one of two records', () => {
            return Promise.all([
                log.create({
                    action: 'shutdown',
                    data: {},
                    entity: null,
                    entity_name: 'system',
                    error: {},
                    type: 'error',
                    updates: [],
                }),
                log.create({
                    action: 'shutdown',
                    data: {},
                    entity: null,
                    entity_name: 'email',
                    error: {},
                    type: 'error',
                    updates: [],
                }),
            ]).then(() => {
                return log.list({
                    createdAt: {
                        $gte: Date.parse(moment().startOf('day').toISOString()),
                        $lte: Date.parse(moment().endOf('day').toISOString()),
                    },
                }, {
                    limit: 1,
                    page: 1,
                    sort: {
                        createdAt: -1,
                    },
                }).then((records) => {
                    expect(records.data.length).to.equal(records.meta.pagination.count);
                });
            });
        });
        it('should find all records up to 25', () => {
            return Promise.all([
                log.create({
                    action: 'shutdown',
                    data: {},
                    entity: null,
                    entity_name: 'system',
                    error: {},
                    type: 'error',
                    updates: [],
                }),
                log.create({
                    action: 'shutdown',
                    data: {},
                    entity: null,
                    entity_name: 'email',
                    error: {},
                    type: 'error',
                    updates: [],
                }),
            ]).then(() => {
                return log.list({
                    createdAt: {
                        $gte: Date.parse(moment().startOf('day').toISOString()),
                        $lte: Date.parse(moment().endOf('day').toISOString()),
                    },
                }, {
                    limit: 25,
                    page: 1,
                    sort: {
                        createdAt: -1,
                    },
                }).then((records) => {
                    expect(records.data.length).to.equal(records.meta.pagination.count);
                });
            });
        });
        it('should find all records', () => {
            return Promise.all([
                log.create({
                    action: 'shutdown',
                    data: {},
                    entity: null,
                    entity_name: 'api',
                    error: {},
                    type: 'error',
                    updates: [],
                }),
                log.create({
                    action: 'shutdown',
                    data: {},
                    entity: null,
                    entity_name: 'cron',
                    error: {},
                    type: 'error',
                    updates: [],
                }),
            ]).then(() => {
                return log.list({
                    createdAt: {
                        $gte: Date.parse(moment().startOf('day').toISOString()),
                        $lte: Date.parse(moment().endOf('day').toISOString()),
                    },
                }, {
                    limit: 0,
                    page: 1,
                    sort: {
                        createdAt: -1,
                    },
                }).then((records) => {
                    expect(records.data.length).to.equal(records.meta.pagination.count);
                });
            });
        });
    });
});

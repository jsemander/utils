import { assert, expect } from 'chai';
import 'mocha';
import * as nodemailer from 'nodemailer';

import { Mail } from '../src/Mail';

let mail: Mail;

describe('Mail', () => {
    before(() => {
        return new Promise((resolve, reject) => {
            nodemailer.createTestAccount((err, account) => {
                if (err) {
                    return reject(err);
                }
                mail = new Mail({
                    from: 'noreply@test.com',
                    transport: {
                        auth: {
                            pass: account.pass,
                            user: account.user,
                        },
                        host: 'smtp.ethereal.email',
                        port: 587,
                        secure: false,
                    },
                });
                return resolve();
            });
        });
    });
    describe('#sendMail()', () => {
        it('should try to send an email successfully', (done) => {
            mail.sendMail({
                from: 'noreply@test.com',
                to: 'me@test.com',
            }).then((res) => {
                assert.ok(true);
                return done();
            }).catch((err) => {
                assert.ok(false, err);
                return done();
            });
        });
        it('should try to send an email unsuccessfully', (done) => {
            mail.sendMail({}).then((res) => {
                assert.ok(false, res);
                return done();
            }).catch((err) => {
                expect(err).to.have.property('message', 'Missing recipient for email');
                return done();
            });
        });
    });
});
